# FrontEndDeveloperMediumSenior

En este archivo encontrara los pasos necesarios para correr el aplicativo desarrollado para la prueba solicitada, también encontrar una explicación de por que se hizo de esta manera la aplicación y la justificación, por último encontrara el link del repositorio creado para esta prueba y se explicara la estructura utilizada


Instalación de Angular 6


Si en su computador tiene angular ya instalado solo debe de ejecutar el siguiente comando.
gn serve
Para que se ejecute el servidor y pueda visualizarlo.


Si no tiene Angular 6 instalado en su computador debe de realizar los siguientes pasos


Paso 1. Instalar la última versión de NodeJS, para eso entraremos a su web oficial y descargaremos el instalador más actualizado https://nodejs.org/es/


Paso 2. Desinstalar los paquetes anteriores de Angular CLI


npm uninstall -g angular-cli
npm uninstall -g @angular/cli
Paso 3. Borrar la caché del gestor de paquetes npm


npm cache verify
npm cache clean --force
Paso 4. Instalar la última versión de Angular CLI


npm install -g @angular/cli@latest
Ahora ya tenemos instalado y actualizado Angular CLI.

Justificación del desarrollo de la aplicación:

La aplicación se realizo de esta manera ya que se tubo encuentra los requerimientos solicitados pero también se tomaron decisiones que mejoraran el diseño propuesto,
basándonos en las reglas manejas en UX y IU.

Se solicitaba que al momento de presionar el color saliera el color grande y la palabra copiado al revisar la estructura y los tipos empleados que ejecutaran esta aplicación 
Debemos de realizarla de una manera fácil de usar y que puedan sacar el contenido necesario rápido, por esa razón se realizo que cuando se presione el botón solo salga un mensaje que confirme la acción y de esta manera es más rápido el proceso con la app y la pueden usar en paralelo con las tareas que deben de ejecutar.

link del repositorio: https://gitlab.com/cristianpt21/frontenddevelopermediumsenior

si se reviza la estructura realizada en el repositorio es GITFLOW se into realizar una estructura basada a un proyecto en produccion, tiene sus ramas de los modulos y tambien de los despliegues y versiones de la app.

