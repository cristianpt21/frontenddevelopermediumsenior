import { Injectable } from '@angular/core';
// importamos HttpClient
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ColoresService {

  constructor(protected http: HttpClient) { }
  getColors() {
    return this.http.get('https://reqres.in/api/colors');
  }
}
