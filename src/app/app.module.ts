import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// Importamos HttpClientModule.
import { HttpClientModule} from '@angular/common/http';
// importamos el servicio de los colores.
import { ColoresService } from './colores.service';
//
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        NgbModule,
        FormsModule
    ],
  providers: [ColoresService],
  bootstrap: [AppComponent]
})
export class AppModule { }
