import {Component, OnInit} from '@angular/core';
// Instanciamos el servicio de los colores.
import { ColoresService } from './colores.service';
// Instanciamos la libreria de modal
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
    .light-blue-backdrop {
      background-color: #5cb3fd;
    }
  `]
})
export class AppComponent implements OnInit {
  title = 'angular6-pruebaFronted';
  colors: any[] = [];
  classStyle = 'showMenssage';
  isMiddleDivVisible: boolean = true;
  constructor(protected colorsService: ColoresService, private modalService: NgbModal) {}
// Cargamos los datos a nuestra variable data.
  ngOnInit() {
    this.colorsService.getColors()
      .subscribe(
        (data) => {
          console.log(data);
          // Success
          this.colors = data['data'];
        },
        error => {
          console.error(error);
        }
      );
  }
  // funcion para copiar el codigo de color.
  copyMessage(val: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.timeout();
  }
  timeout() {
    setTimeout(function () {
      console.log('codigo copiado correctamente');
    }, 1000/60);
  }

}
